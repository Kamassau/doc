### [SLURM] `Illegal instruction (core dumped)` on some nodes

#### Explanation

We have different generations of computing nodes with different brands and generations of CPU.

It can result that some of our CPU have the needed [instruction set architecture](https://en.wikipedia.org/wiki/Instruction_set_architecture) for your software and some not (too old?).

##### Solution

It's possible to select a set of nodes with some `--constraint` based on the nodes `Features`:

- Vendor: `intel` or `amd`
- CPU familly: `broadwell`, `haswell`, `epyc_rome` ...
- CPU instruction set: `avx2`

To get the features available on the node:
```
$ sinfo -e --format "%.26N %.4c %.7z %.7m %.20G %f"
                  NODELIST CPUS   S:C:T  MEMORY                 GRES AVAIL_FEATURES
               n[56-57,59]   48  2:24:1  257000               (null) intel,haswell,avx2
  n[70,73,77,83,85,95,113]   96  2:48:1  257000               (null) amd,epyc_rome,avx2
               n[75,96-98]  256 2:128:1  257000               (null) amd,epyc_rome,avx2
               n[76,78-79]   48  4:12:1  257000               (null) amd,opteron_k10
                n[115-118]   48  2:24:1  257000               (null) intel,broadwell,avx2
                       n99   80  4:20:1 1032000               (null) intel,westmere
                      n100  128  4:32:1 2064000               (null) intel,broadwell,avx2
               gpu-node-01   40  2:20:1  128000     gpu:k80:2(S:0-1) intel,broadwell,avx2
```

Thus, you can for example target nodes that allow `avx2` and from `amd`

```
#SBATCH --constraint=avx2&amd
```

💡 If you think that some features are needed, don't hesitate to contact us.

### [R] `Error: Fatal error: cannot create 'R_TempDir'`

#### Explanation 1

The server on which the job ran must have a full on its `/tmp/`. Indeed, by default, R by default, is writing temporary files in the `/tmp/`  directory of the server.

The local directory `/tmp/` is limited and shared. It's not a good practice to let a software writing on local disk.

##### Solution

The solution is to change the default temporary directory of R.

Please add the following lines at the beginning of your `sbatch` script.
```bash
#!/bin/bash
# SBATCH -p fast

TMPDIR="${DATA_DIR}/tmp"
mkdir -p "${TMPDIR}"
TMP="${TMPDIR}"
TEMP="${TMPDIR}"
export TMPDIR TMP TEMP

module load r/4.1.1
Rscript my_script.R
```
