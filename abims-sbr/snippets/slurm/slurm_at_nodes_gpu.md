
(Last update: 04/05/2022)


### GPU nodes

| Nbr | GPU | CPU | RAM (GB )| Type | Disk /tmp |
| --:| --:| --:| --:| --:| --:|
| 1 | 2 | 40 | 128 | [NVIDIA k80](https://www.nvidia.com/fr-fr/data-center/tesla-k80/) | 32 GB |

##### GPU Instance Profiles

⚠️ The values below can change. To check the current situation:
```bash
sinfo -Ne -p gpu --format "%.15N %.4c %.7m %G"
```
| Profile Name | GPU Memory  | Number of Instances Available |
| --:| --:| --:|
| k80 | 24GB | 2 | 

