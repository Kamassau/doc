# Handle your data

## Storage

{% include "/data/storage.md" %}

{% include "/data/data_backup.md" %}


## Project

A project is a directory on the storage associated with users who can access it.

In order to have your folder and space on **`/shared/projects/<myproject\>`**, you have to request a new project on the IFB Core Cluster Account Manager: [{{ account_manager.url_project }}]({{ account_manager.url_project }})

You can have multiple projects (and it's recommended).

More information on [The project concept at {{ platform.name }}](project.md)


## Transfer

SSH protocol (or SFTP - SSH File Transfer Protocol) is the only protocol available.
But, you can also use clients on the cluster to download your data (*wget*, *curl*, *scp*, *rsync*, *wget*, *ftp*, etc.).

You can get/put your data as described below.

### Command line

On your workstation or on the cluster, you can use command line with your favorite client: scp, rsync, etc.
<asciinema-player src="../../_casts/transfer.cast"  id="asciicast-transfer" async poster="npt:0:11" speed="1.5" preload="true" data-autoplay="false"></asciinema-player>

### SFTP Graphic client

Use a graphic client like [FileZilla](https://filezilla-project.org/) on your workstation.  
<video width="572" controls><source src="../../_casts/filezilla_lowquality.mp4" type="video/mp4">Your browser does not support the video tag.</video>

Or use your file manager directly if it's possible (SFTP support).
<video width="572" controls><source src="../../_casts/sftp_ubuntu-18-04.mp4" type="video/mp4">Your browser does not support the video tag.</video>

### SSHFS

Use your file manager with [SSHFS](https://github.com/libfuse/sshfs)  
<video width="572" controls><source src="../../_casts/sshfs.mp4" type="video/mp4">Your browser does not support the video tag.</video>


## Editors

Several editors and tools are available on the cluster:

* `vim`
* `emacs`
* `nano`

Or graphical editors. Graphic tools need some configuration: see [Export display](data.md#export-display).

* `xemacs`
* `geany`
* `gedit`

Or use web interface:

* [RStudio]({{ rstudio.url }})
{% if jupyterhub.url is defined %}
* [JupyterHub]({{ jupyterhub.url }})
{% endif %}



### Export display

* Linux / Mac  
  Simply use `-Y` option:  
  ```bash
  ssh -Y <username>@{{ cluster.url }}
  ```

* Windows  
  1\. Install a X server (for example: [VcXsrv Windows X Server](https://sourceforge.net/projects/vcxsrv/))  
  2\. Launch the X server  
  3\. Activate X11 forwarding  
  <a onclick="toggle_visibility('putty_x11forwarding');">> Example with PuTTY</a>  
  <img id="putty_x11forwarding" class="hide" src="../_imgs/putty_x11forwarding_highlighting.png" alt="X11 Forwarding with PuTTY"></img>  

Once connected, you can launch your program with a graphical user interface.


## Manage

Several possibilities to browse, manage and edit or view your data/scripts

* Directly on the cluster with the command line

* Get the data back on your workstation (see [Transfer](../quick-start.md#transfer)) and work locally.

* Use SSHFS on your workstation (see above: [SSHFS](data.md#sshfs))

