# Troubleshooting

💡 The following Troubleshooting can be completed by consulting the [IFB Community Forum](https://community.france-bioinformatique.fr/)

### [SLURM] `Invalid account or account/partition combination specified`

Complete message:
```
srun: error: Unable to allocate resources: Invalid account or account/partition combination specified
```

#### Explanation 1
Your current default SLURM account should be the **`demo`** one. You may have seen a red notice at login? You can check that using:

```bash
$ sacctmgr list user $USER
      User   Def Acct     Admin
---------- ---------- ---------
   cnorris       demo      None
```

##### Solution

If you don't already have a project, you have to request one from the platform: [{{ account_manager.url_project }}]({{ account_manager.url_project }})

Otherwise, you already have a project/account, you can either:

- Specify at each job your SLURM account:
```bash
srun -A my_account command
```
```bash
#!/bin/bash
#SBATCH -A my_account
command
```
- Change your default account
```bash
sacctmgr update user $USER set defaultaccount=my_account
```
⚠️ status_bar is updated hourly. So it may still display demo as your default account by don't worry, it should have work.


{% include "troubleshooting_pf.md" %}
