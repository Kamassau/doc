#!/bin/bash

# Cleaning
bash <(sed "s/^/rm -rf /" .gitignore)

DOMAIN=$1

#cp ${DOMAIN}/docs/*.md docs/
cp ${DOMAIN}/docs/_imgs/* docs/_imgs/PF/
cp ${DOMAIN}/snippets/*.md snippets/
cp ${DOMAIN}/snippets/data/* snippets/data/
cp ${DOMAIN}/snippets/slurm/*.md snippets/slurm/
cp ${DOMAIN}/snippets/software/*.md snippets/software/

mkdocs build -f mkdocs_${DOMAIN}.yml
mkdir site/tutorials
for tutorial in tutorials/*.md; do claat export -o site/tutorials ${tutorial}; done

mv site public