
(Last update: 04/05/2022)

### CPU nodes

| Nbr | CPU (Hyper-Thread)| RAM (GB )|
| --:| --:| --:|
| 10 | 30 | 128 |
| 1 | 38 | 256 |
| 66 | 54 | 256 |
| 1 | 124 | 3096 |

